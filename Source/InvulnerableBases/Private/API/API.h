#ifndef API_H
#define API_H

#include <string>
#include <stdlib.h>
#include <Windows.h>
#include <string>
#include <vector>

using namespace std;

#define ARKAPI __declspec(dllimport)
#define EXTERN_C     extern "C"

class ConsoleAPI {
public:
	static enum Color {
		DEFAULT = 0x100,
		GREEN = 0x0A,
		RED = 0x0C,
		BLUE = 0x09,
		YELLOW = 0x0E,
		WHITE = 0x0F
	};

	static enum LogType {
		LOG_ERROR,
		LOG_WARNING,
		LOG_INFO
	};

};

struct API
{
	// Console Functions
	virtual void ConsoleMessage(char* p[128], char* s[4096]);
	virtual void ConsoleMessage(char* p[128], char* s[4096], ConsoleAPI::Color c);
	virtual void ConsoleMessage(char* p[128], char* s[4096], ConsoleAPI::LogType t);
	virtual void Emit(string n, string d);

	// Release API
	virtual void Release() = 0;
};


class ARKAPI ArkAPI
{
public:
	void ConsoleMessage(char* p[128], char* s[4096]);
	void ConsoleMessage(char* p[128], char* s[4096], ConsoleAPI::Color c);
	void ConsoleMessage(char* p[128], char* s[4096], ConsoleAPI::LogType t);
	void Emit(string n, string d);
};

// Handle type. In C++ language the iterface type is used.
typedef API* APIHANDLE;
extern "C" ARKAPI API* APIENTRY GetAPI(VOID);

#endif

class PluginAPI {
public:

	static void ConsoleMessage(string message);
	static void ConsoleMessage(string message, ConsoleAPI::Color color);
	static void ConsoleMessage(string message, ConsoleAPI::LogType logType);
};