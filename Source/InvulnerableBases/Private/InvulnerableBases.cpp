// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "InvulnerableBasesPrivatePCH.h"

class FInvulnerableBases : public IInvulnerableBases
{
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};

FInvulnerableBases cInvulnerableBases;

IMPLEMENT_MODULE( FInvulnerableBases, InvulnerableBases )

void FInvulnerableBases::StartupModule()
{
	// This code will execute after your module is loaded into memory (but after global variables are initialized, of course.)

}

void FInvulnerableBases::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
	Plugin::Unload();
}

bool ProcessAttach(HINSTANCE hModule)
{
	Plugin::Load();
	return true;
}

bool ProcessDetach(HINSTANCE hModule)
{
	Plugin::Unload();
	return true;
}

void term_func()
{
	printf("Unhandled exception");
}

bool APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	set_terminate(term_func);
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		return ProcessAttach(hModule);
	case DLL_PROCESS_DETACH:
		return ProcessDetach(hModule);
	}
	return true;
}
